<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Models\Master\EventModel;

use App\Http\Services\Master\EventService;

class EventController extends Controller
{
    protected $eventService;
    
    public function __construct(EventService $eventServices){
        $this->eventServices = $eventServices;
    }

    public function upcomingEvent(Request $request)
    {
        $eventList = $this->eventServices->read($request);
        Log::info($request);

        $eventModel = new EventModel();
        $eventList = $eventModel::where('event_start', '>', $request->current_date)->get();
        return response()->json(['data' => $eventList]);
    }
    
    public function pastEvent(Request $request)
    {
        // $eventList = $this->eventServices->read($request);
        Log::info($request);

        $eventModel = new EventModel();
        $eventList = $eventModel::where('event_end', '<', $request->current_date)->get();
        return response()->json(['data' => $eventList]);
    }
}
