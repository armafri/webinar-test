<?php

namespace App\Http\Services\Master;

use Illuminate\Http\Request;
use App\Models\Master\EventModel;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

class EventService
{
    protected $eventModel;

    public function __construct(EventModel $eventModel)
    {
        $this->eventModel = $eventModel;
    }

    public function read(Request $request)
    {
        return $this->eventModel::All();
    }

    public function detail($id)
    {
        return $this->eventModel::where(['id' => $id])->first();
    }

    // public function create(Request $request)
    // {
    //     $EventModel = $this->EventModel;

    //     $EventModel->EventCode = $request->EventCode;
    //     $EventModel->name = $request->name;
    //     $EventModel->price = $request->price;
        
    //     $result = 0;
    //     if($EventModel->save()){
    //         $result = 1;
    //     }
    //     return $result;
    // }
    
    // public function update(Request $request, $id)
    // {
    //     $EventModel = $this->EventModel;

    //     $param = [
    //         'name' => $request->name,
    //         'price' => $request->price
    //     ];

    //     $result = 0;
    //     if($EventModel::where('EventCode', $request->EventCode)->update($param)){
    //         $result = 1;
    //     }
        
    //     return $result;
    // }
}
