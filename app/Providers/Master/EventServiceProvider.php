<?php

namespace App\Providers\Master;

use Illuminate\Support\ServiceProvider;
use App\Models\Master\EventModel;
use Illuminate\Http\Request;

class EventServiceProvider extends ServiceProvider
{
    protected $eventModel;

    public function __construct(EventModel $eventModel){
        $this->eventModel = $eventModel;
    }

    public function read(Request $request){
        $eventList = $this->eventModel::all();
        return $eventList;
    }
}
