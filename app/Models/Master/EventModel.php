<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use DB;

class EventModel extends Model
{
	use HasFactory, Notifiable;

	protected $table = 'events';
	protected $fillable=[
		'id',
        'title',
        'description',
        'event_start',
        'event_end',
        'price',
        'created_at',
        'updated_at'
	];
}